<?php 

namespace App\Models;
use CodeIgniter\Model;

class Usuario extends Model
{

	protected $table = 'usuario';
	protected $primaryKey = 'id_usuario';

	public function getUsuarios($id=null)
	{
		if ($id === null) {
			return $this->findAll();
		}

		return $this->asArray()->where(['id_usuario' => $id])->first();
	}

	public function getUsuarios($id=null)
	{
		if ($id === null) {
			return $this->findAll();
		}
	}

	//--------------------------------------------------------------------

}
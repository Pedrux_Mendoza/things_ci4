<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\Usuario;

class User extends Controller
{
	public function index()
	{
		$usuario = new Usuario();

		$data =  [
			'usuarios' => $usuario->getUsuarios()
		];
		//return view('welcome_message');

		echo view('templates/header');
		echo view('usuario/overview', $data);
		echo view('templates/footer');
	}

	public function view($id=null)
	{
		$usuario = new Usuario();
		$data['usuarios'] = $usuario->getUsuarios($id);

		if (empty($data['usuarios'])) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException("No se puede encontrar este item: ". $id);
		}

		//$data['username'] = $data['usuarios']['username'];

		echo view('templates/header');
		echo view('usuario/view', $data);
		echo view('templates/footer');

	}

	public function create()
	{
		helper('form');

		echo view('templates/header');
		echo view('usuario/form');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

}
<body>
<h2><?php echo isset($id) ? "Editando Usuario" : "Nuevo Usuario"; ?></h2>
<form action="/crud_ci4/public/User/insertar" method="post">
	<div class="form-group">
		<label for="username">Usuario</label>
		<input type="text" name="username" id="username" class="form-control">
	</div>
	<div class="form-group">
		<label for="password">Contraseña</label>
		<input type="text" name="password" id="password" class="form-control">
	</div>
	<div class="form-group">
		<label for="email">Correo</label>
		<input type="text" name="email" id="email" class="form-control">
	</div>			
</form>

<body>
<script>
	function confirmar() {
		if (!confirm("Desea realemnte eliminar este dato?")) {
			return false;
		}

		return true;
	}
</script>
<h2>Usuarios</h2>
<div class="my-3">
	<a class="btn btn-success" href="/crud_ci4/public/User/create">Nuevo Usuario</a>
</div>
<table class="table">
	<tr>
		<th>Usuario</th>
		<th>Correo</th>
		<th colspan="2">ID</th>
	</tr>
	<?php if (!empty($usuarios) && is_array($usuarios)) { ?>
			<?php foreach ($usuarios as $users_items) { ?>
				<tr>
					<td><a href="/crud_ci4/public/User/view/<?php echo $users_items['id_usuario'];  ?>"><?php echo $users_items['username']; ?></a></td>
					<td><?php echo $users_items['email']; ?></td>
					<td><a class="btn btn-danger" onclick="return confirmar()" href="/crud_ci4/public/User/delete/<?php echo $users_items['id_usuario'];  ?>">Eliminar</a></td>
					<td><a class="btn btn-info" href="/crud_ci4/public/User/edit/<?php echo $users_items['id_usuario'];  ?>">Editar</a></td>
				</tr>
			<?php }  ?>
	<?php }else{ ?>
		<tr>
			<td colspan="3">Ningun usuario encontrado</td>
		</tr>
	<?php } ?>
</table>
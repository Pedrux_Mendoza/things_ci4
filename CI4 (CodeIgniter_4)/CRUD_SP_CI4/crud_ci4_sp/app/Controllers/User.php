<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\Usuario;

class User extends Controller
{
	public function index()
	{
		$usuario = new Usuario();

		$data['usuarios'] = $usuario->getUsuarios();
		$data['competencia'] = $usuario->getCompetencia();
		echo view('templates/header', $data);
		echo view('usuario/overview');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

	public function delete($id)
	{
		$usuario = new Usuario();

		$msj = $usuario->eliminar($id);

		if ($msj == true) {
			echo "<script>alert('Eliminado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Eliminar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_sp/public/User');
	}

	//--------------------------------------------------------------------	

	public function insertar()
	{
		$usuario = new Usuario();

		$datos['usuario'] = $this->request->getPost('username');
		$datos['contraseña'] = md5($this->request->getPost('password'));
		$datos['correo'] = $this->request->getPost('email');
		$datos['competencia'] = $this->request->getPost('competencia');

		$msj = $usuario->insertar($datos);

		if ($msj == "add") {
			echo "<script>alert('Agregado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Agregar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_sp/public/User');
	}

	//--------------------------------------------------------------------

	public function getDatos($id)
	{
		$usuario = new Usuario();

		$data['usuarios'] = $usuario->getDatos($id);
		$data['competencia'] = $usuario->getCompetencia();
		echo view('templates/header', $data);
		echo view('usuario/form');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

	public function actualizar()
	{
		$usuario = new Usuario();

		$datos['id'] = $this->request->getPost('id');
		$datos['usuario'] = $this->request->getPost('username');
		$datos['correo'] = $this->request->getPost('email');
		$datos['competencia'] = $this->request->getPost('competencia');

		$msj = $usuario->actualizar($datos);

		if ($msj == "modi") {
			echo "<script>alert('Modificado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Modificar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_sp/public/User');
	}

}
<?php 

namespace App\Models;
use CodeIgniter\Model;
use App\Entities\Logeo;

class LoginM extends Model
{
	public function validar($login)
	{
		$sp_comprobar_usuario = 'CALL sp_comprobar_usuario(?,?)';
		$query = $this->query($sp_comprobar_usuario, [$login->getUsername() ,$login->getPassword()]);

		if (count($query->getResult())>0) {

			$usuario = array();

			foreach ($query->getResult() as $row) {
				$ax = new Logeo();
				$ax->setUsername($row->username);
				$ax->setPassword($row->password);
				array_push($usuario, $ax);
			}

			return $usuario;
		}else{
			return false;
		}
	}

	//--------------------------------------------------------------------

}
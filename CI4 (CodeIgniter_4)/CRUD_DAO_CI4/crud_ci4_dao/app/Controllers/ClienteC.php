<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\Cliente;
use App\Models\Cliente;
 class ClientesC extends Controller
 {
 	public function index()
 	{
 		$cliente = new ClienteM();

 		echo view('template/header');
 		echo view('template/navbar');
 		echo view('cliente/overview');
 		echo view('template/footer');
 	}

 	public function insertarC()
 	{
 		$cliente = new ClienteM();
 		

 		$Cliente = new Cliente();
 		$data =[
 			'dui'=> $this->request->getPost('dui'),
 			'nit'=> $this->request->getPost('nit'),
 			'nombres'=> $this->request->getPost('nombres'),
 			'apellido'=> $this->request->getPost('apellido'),
 			'direccion'=> $this->request->getPost('direccion'),
 			'telefono'=> $this->request->getPost('telefono')
 			];

 			$Cliente->fill($data);

 		if ($cliente->save($Cliente) == true) {
			echo "<script>alert('Agregado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Agregar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_dao/public/ClienteC');
	}
 	}

 }
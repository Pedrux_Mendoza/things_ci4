<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\User;
use App\Models\Usuario;

class Usuarios extends Controller
{
	public function index()
	{
		$usuario = new Usuario();

		$data['usuarios'] = $usuario->getUsuarios();
		/*$result = $usuario->get_Competencia();*/ //Descometarear cuando se utiliza 2 entidades
		$data['competencia'] = $usuario->get_Competencia();
		echo view('templates/header', $data);
		echo view('usuario/overview');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

	public function delete($id)
	{
		$usuario = new Usuario();

		$msj = $usuario->eliminar($id);

		if ($msj == true) {
			echo "<script>alert('Eliminado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Eliminar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_dao/public/Usuarios');
	}

	//--------------------------------------------------------------------

	public function insertar()
	{
		$usuario = new Usuario();

		// Create
		$user = new User();
		$user->setUsername($this->request->getPost('username'));
		$user->setEmail($this->request->getPost('email'));
		$user->setPassword($this->request->getPost('password'));
		$user->setIdCompetencia($this->request->getPost('competencia'));

		$msj = $usuario->insertar($user);

		if ($msj == "add") {
			echo "<script>alert('Agregado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Agregar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_dao/public/Usuarios');
	}

	//--------------------------------------------------------------------

	public function getDatos($id)
	{
		$usuario = new Usuario();

		$data['usuarios'] = $usuario->getUsuario($id);
		/*$result = $usuario->get_Competencia();*/ //Descometarear cuando se utiliza 2 entidades
		$data['competencia'] = $usuario->get_Competencia();
		echo view('templates/header', $data);
		echo view('usuario/form');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

	public function actualizar()
	{
		$usuario = new Usuario();

		// Create
		$user = new User();
		$user->setIdUsuario($this->request->getPost('id'));
		$user->setUsername($this->request->getPost('username'));
		$user->setEmail($this->request->getPost('email'));
		$user->setIdCompetencia($this->request->getPost('competencia'));

		$msj = $usuario->actualizar($user);

		if ($msj == true) {
			echo "<script>alert('Modificado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Modificar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_dao/public/Usuarios');
	}

	//--------------------------------------------------------------------	
}

<?php 

namespace App\Models;
use CodeIgniter\Model;
use App\Entities\User;
use App\Entities\Competition;

class Usuario extends Model
{
	public function getUsuarios()
	{
		$sp_consultar_uc = 'CALL sp_consultar_uc()';
		$query = $this->query($sp_consultar_uc);

		$competencia = array();

		foreach ($query->getResult() as $row) {
			$ax = new User();
			$ax->setIdUsuario($row->id_usuario);
			$ax->setUsername($row->username);
			$ax->setEmail($row->email);
			$ax->setIdCompetencia($row->nombre_competencia);
			array_push($competencia, $ax);
		}

		return $competencia;
	}

	//--------------------------------------------------------------------

	public function eliminar($id)
	{
		$sp_eliminar_usuario = 'CALL sp_eliminar_usuario(?)';
		$query = $this->query($sp_eliminar_usuario, [$id]);

		if ($query) {
			return true;
		}else{
			return false;
		}
	}

	//--------------------------------------------------------------------

	public function get_Competencia()
	{
		$sp_consultar_competencias = 'CALL sp_consultar_competencias()';
		$query = $this->query($sp_consultar_competencias);

	//Cuando se necesita guardar la informacion en el arreglo de identidad
	/*	$x = new User();

		foreach ($query->getResult() as $row) {
			$ax = new Competition();
			$ax->setIdCompetencia($row->id_competencia);
			$ax->setNombreCompetencia($row->nombre_competencia);
			$ax->setDescripcion($row->descripcion);
			$x->setCompetencia($ax);
		} */

		$competencia = array();

		foreach ($query->getResult() as $row) {
			$ax = new Competition();
			$ax->setIdCompetencia($row->id_competencia);
			$ax->setNombreCompetencia($row->nombre_competencia);
			$ax->setDescripcion($row->descripcion);
			array_push($competencia, $ax);
		}

		return $competencia;
	}

	//--------------------------------------------------------------------

	public function insertar($user)
	{
		$sp_insertar_usuario = 'CALL sp_insertar_usuario(?, ?, ?, ?)';
		$query = $this->query($sp_insertar_usuario, [$user->getUsername() ,$user->getPassword(),$user->getEmail() ,$user->getIdCompetencia()]);

		if ($query) {
			return "add";
		}else{
			return "errorA";
		}
	}

	//--------------------------------------------------------------------	

	public function getUsuario($id)
	{
		$sp_consultarporid = 'CALL sp_consultarporid(?)';
		$query = $this->query($sp_consultarporid, [$id]);

		if ($query) {
			foreach ($query->getResult() as $row)
				{
				    // Create
					$user = new User();
					$user->setIdUsuario($row->id_usuario);
					$user->setUsername($row->username);
					$user->setEmail($row->email);
					$user->setPassword($row->password);
					$user->setIdCompetencia($row->id_competencia);

					return $user;
				}
		}else{
			return false;
		}
	}

	//--------------------------------------------------------------------

	public function actualizar($user)
	{
		$sp_modificar_usuario = 'CALL sp_modificar_usuario(?, ?, ?, ?)';
		$query = $this->query($sp_modificar_usuario, [$user->getIdUsuario() ,$user->getUsername(),$user->getEmail() ,$user->getIdCompetencia()]);

		if ($query) {
			return "modi";
		}else{
			return "errorM";
		}
	}

}
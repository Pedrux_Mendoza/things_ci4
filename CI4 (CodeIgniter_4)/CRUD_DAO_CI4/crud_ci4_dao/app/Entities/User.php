<?php 

namespace App\Entities;
use CodeIgniter\Entity;

class User extends Entity 
{
    private $id_usuario;
    private $username;
    private $email;
    private $password;
    private $id_competencia;
    //Array de Instancias
    private $competencia = array();

    /*Rel*/
    public function getCompetencia()
    {
        return $this->competencia;
    }

    public function setCompetencia($competencia)
    {
    	if ($competencia instanceof Competition) {
    		$this->competencia[] = $competencia;
    	}
    }    

    /*G&S*/
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = md5($password);
    }

    public function getIdCompetencia()
    {
        return $this->id_competencia;
    }

    public function setIdCompetencia($id_competencia)
    {
        $this->id_competencia = $id_competencia;
    }
}
-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-01-2020 a las 23:24:26
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `equipo12`
--
CREATE DATABASE IF NOT EXISTS `equipo12` DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE `equipo12`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_comprobar_usuario` (IN `pa_username` VARCHAR(50), IN `pa_password` VARCHAR(50))  BEGIN
SELECT u.id_usuario, u.username, u.email, c.nombre_competencia, u.password FROM usuario u
INNER JOIN competencia c ON u.id_competencia = c.id_competencia WHERE (u.username = pa_username AND u.password = pa_password);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultarporid` (IN `pa_id_usuario` INT)  BEGIN
SELECT * FROM usuario WHERE id_usuario = pa_id_usuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_competencias` ()  BEGIN
SELECT * FROM competencia;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_uc` ()  BEGIN
SELECT u.id_usuario, u.username, u.email, c.nombre_competencia FROM usuario u
INNER JOIN competencia c ON u.id_competencia = c.id_competencia;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar_usuario` (IN `pa_id_usuario` INT)  BEGIN
DELETE FROM `usuario` WHERE id_usuario = pa_id_usuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_usuario` (IN `pa_username` VARCHAR(50), IN `pa_password` VARCHAR(50), IN `pa_email` VARCHAR(100), IN `pa_id_competencia` INT)  BEGIN
INSERT INTO `usuario`(`username`, `password`, `email`, `id_competencia`) VALUES (pa_username, pa_password, pa_email, pa_id_competencia);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_modificar_usuario` (IN `pa_id_usuario` INT, IN `pa_username` VARCHAR(50), IN `pa_email` VARCHAR(100), IN `pa_id_competencia` INT)  BEGIN
UPDATE `usuario` SET 	`username`=pa_username,
                        `email`=pa_email,
                        `id_competencia`=pa_id_competencia
WHERE `id_usuario`=pa_id_usuario;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencia`
--

CREATE TABLE `competencia` (
  `id_competencia` int(11) NOT NULL,
  `nombre_competencia` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `competencia`
--

INSERT INTO `competencia` (`id_competencia`, `nombre_competencia`, `descripcion`) VALUES
(1, 'Trabajo en equipo', 'Requiere trabajar en proyectos comunes a varios empleados, la empresa necesita asegurarse de que estás abierto a escuchar distintas ideas y a recibir críticas constructivas en beneficio de los objetivos de la organización.'),
(2, 'Comunicación interpersonal', 'Saber comunicarte con los demás, tanto de manera interna como externa a la empresa. Lidiar con clientes, empleados y superiores de manera correcta es una habilidad que no todo el mundo posee.'),
(3, 'Liderazgo', 'Es importante remarcar que sabes tomar decisiones, resolver problemas y motivar a otros de manera efectiva.'),
(4, 'Organización', 'Saber administrar el tiempo y organizar las tareas en consecuencia te presentará como un empleado confiable y eficiente.'),
(5, 'Análisis', 'Poseer una buena capacidad de análisis de los números y las estadísticas es esencial para obtener mejores resultados de negocio.'),
(6, 'Expresión escrita', 'Saber expresarte de forma escrita es una virtud extremadamente importante. Esta habilidad debe ser demostrada de forma implícita en tu hoja de vida, asegurándote de que cada línea carezca de errores sintácticos u ortográficos.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `username` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `id_competencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `username`, `password`, `email`, `id_competencia`) VALUES
(18, 'pedro', '202cb962ac59075b964b07152d234b70', 'pedrux_mendoza@hotmail.com', 6);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `competencia`
--
ALTER TABLE `competencia`
  ADD PRIMARY KEY (`id_competencia`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `usuario_competencia` (`id_competencia`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `competencia`
--
ALTER TABLE `competencia`
  MODIFY `id_competencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_competencia` FOREIGN KEY (`id_competencia`) REFERENCES `competencia` (`id_competencia`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

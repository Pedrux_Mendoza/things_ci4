-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-02-2020 a las 18:18:50
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aficiones`
--
CREATE DATABASE IF NOT EXISTS `aficiones` DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE `aficiones`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hobby`
--

CREATE TABLE `hobby` (
  `hobby_id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `id_joven` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `hobby`
--

INSERT INTO `hobby` (`hobby_id`, `nombre`, `id_joven`) VALUES
(0, 'jugar tenis', 3),
(1, 'Videojuegos', 1),
(3, 'jugar ajedrez', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `joven`
--

CREATE TABLE `joven` (
  `joven_id` int(11) NOT NULL,
  `nombre` varchar(15) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `joven`
--

INSERT INTO `joven` (`joven_id`, `nombre`) VALUES
(3, 'Daniel'),
(1, 'Ernesto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario_id` int(11) NOT NULL,
  `user` varchar(11) COLLATE latin1_spanish_ci NOT NULL,
  `pass` varchar(4) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario_id`, `user`, `pass`) VALUES
(1, '3rn3sto', '1287'),
(2, 'rudy', '123'),
(3, 'pedrux', '1234');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`hobby_id`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD KEY `id_joven` (`id_joven`);

--
-- Indices de la tabla `joven`
--
ALTER TABLE `joven`
  ADD PRIMARY KEY (`joven_id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario_id`),
  ADD UNIQUE KEY `user` (`user`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `hobby`
--
ALTER TABLE `hobby`
  ADD CONSTRAINT `hobby_ibfk_1` FOREIGN KEY (`id_joven`) REFERENCES `joven` (`joven_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

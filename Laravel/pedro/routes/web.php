<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'Logins@index');
Route::post('/verificar', 'Logins@verificar');
Route::get('/dashboard', 'Logins@dashboard');
Route::post('/agregar', 'Logins@insertar');
Route::get('/logout', 'Logins@cerrar');
Route::get('/lista', 'Logins@lista');
Route::get('/crearpdf', 'Logins@downloadPDF');
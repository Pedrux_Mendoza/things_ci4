<?php

namespace App\Http\Controllers;
//LLAMANDO A MI CLASE LOGINPROCEDURE
use App\Procedures\LoginProcedure;
use Illuminate\Http\Request;
use PDF;

class Logins extends Controller
{
	//Se almacenará en una propiedad  heredable.
    protected $LoginProcedure;
    //En el método constructor, se pasará para poder utilizarlo cuando desee dentro de la clase.
    /*Fn: Constructor de la Clase
    @param: Objeto de tipo LoginProcedure
    @return: na */    
    public function __construct(LoginProcedure $LoginProcedure)
    {
        $this->LoginProcedure = $LoginProcedure;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    
    /*Fn: Mostrar vista principal del Login
    @param: Objeto de tipo Request
    @return: Vista Principal */     
    public function index(Request $request)
    {
	/* Para determinar si el elemento Loguerado está presente en la sesión, usamos el método has. El método has devuelve true si el valor está presente y si no es null. */
    	if ($request->session()->has('Logueado')) {
			return redirect()->action('Logins@dashboard');	
		}else{
			return view('login.login');    
		}
    }

    /*Fn: Mostrar vista principal de las aficiones
    @param: Objeto de tipo Request
    @return: Vista Aficiones */     
    public function dashboard(Request $request)
    {
	/* Para determinar si el elemento Loguerado está presente en la sesión, usamos el método has. El método has devuelve true si el valor está presente y si no es null. */    	
    	if ($request->session()->has('Logueado')) {
        	//Almaceno el resultado en una variable $jovenes, llamo a la propiedad y le pido el método que llamara a la consulta    		
    		$jovenes = $this->LoginProcedure->getJoven();
        	/*Se lo manda finalmente a una vista, con la variable :) esta se volvió en un arreglo asociativo, así que solo tendría que llamar a los campos desde la vista algo así {{$departamentos[0]->Codigo_Departamento}} y hacerle un foreach */    		
        	return view('login.dashboard', compact('jovenes'));  	
		}else{
            toastr()->error('Por favor, Inicie Sesion Primero', 'Error!');
			return redirect()->action('Logins@index');
		}
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*Fn: Verifico si existe el usuario ingresado
    @param: Objeto de tipo Request
    @return: Vista Login o de Afciones */     
    public function verificar(Request $request)
    {
    	//Almaceno el resultado en una variable $num, llamo a la propiedad y le pido el método que llamara a la consulta y traera el numero de usuarios
        $num = $this->LoginProcedure->Verificar($request->usuario, $request->contra);
        if ($num > 0) {
        	/*Si el usuario existe saldra un valor mayor que 0 y crearemos la sesion utilizando un arreglo para guardar los valores almacenados en la sesion*/
        	//Para almacenar datos en la sesión, típicamente usarás el método put
			/* $request->session()->put('usuario', $request->usuario); */
			// o el helper session
			session(['usuario' => $request->usuario, 'contra' => $request->contra, 'Logueado' => true]);
			//Ejemplo de como obtener el valor de la session en el controlador
			$user = $request->session()->get('usuario');
			//Imprimimos el valor del usuario en el toastr
        	toastr()->success($user.' Ha Iniciado Sesion', 'Exito!');
        	return redirect()->intended('dashboard');
        }else{
        	alert()->error('Error al ingresar', 'Error!')->persistent('Close');
	        return redirect()->action('Logins@index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*Fn: Crea una nueva aficion
    @param: Objeto de tipo Request
    @return: Vista Aficion */       
    public function insertar(Request $request)
    {
    	//Declaramos el contador para poder buscar un ID inicializado en 0
    	$contador = 0;
    	/*Entramos a un bucle para poder buscar el id que se encuentre en el DB y le pido el método que llamara a la consulta y traera si ese ID se encuentra disponible*/
    	//Mietras el ID de hobbie se encuentre ya en uso
    	do  {
            $contador++;		//Se aumentara en 1
    		//Almaceno el resultado en una variable $id, llamo a la propiedad y le pido el método que llamara a la consulta para comprobar el id y traera el booleano
            $id = $this->LoginProcedure->VerificarID($contador);
        }while($id == "true");	//Finalizara hasta que encuentre un false en este ciclo
        //Llamo a la propiedad y le pido el método que llamara a la insercion del resultador del contador asi como tambien los datos que mando a pedir con el form de agregar hobbies
        $this->LoginProcedure->insertAficion($contador,$request->hobbie,$request->joven);
        alert()->success('', 'Aficion Ingresado con exito')->persistent('Ok');
        return redirect()->action('Logins@dashboard');
    }
    /*Fn: Cierra Sesion de un usuario ingresado
    @param: Objeto de tipo Request
    @return: Vista Principal de Login */   
    public function cerrar(Request $request)
    {
    	//Removemos todos los datos de la sesión, utilizando el método flush
    	$request->session()->flush();
        return redirect()->action('Logins@index');
    }

    public function downloadPDF() {
        //Almaceno el resultado en una variable $fila, llamo a la propiedad y le pido el método que llamara a la consulta de mis compañeros en fila en la base de datos
        $fila = $this->LoginProcedure->getCompañerosFilas();
        //Para generar un PDF a partir de una vista Blade usamos el método loadView el cual recibirá como primer parámetro la ubicación de nuestra vista Blade sin especificar la extensión y lo guardamos en una variable
        $pdf = PDF::loadView('login.pdf', compact('fila'));
        //Para evitar que el documento se descargue solo debemos utlizar el método stream utilizanod la variable $pdf
        return $pdf->stream('lista.pdf');
    }

    /*Fn: Mostrar vista principal de las aficiones de mis compañeros de fila
    @param: Objeto de tipo Request
    @return: Vista Compañeros de filas con sus aficiones */     
    public function lista(Request $request)
    {
    /* Para determinar si el elemento Loguerado está presente en la sesión, usamos el método has. El método has devuelve true si el valor está presente y si no es null. */     
        if ($request->session()->has('Logueado')) {
            //Almaceno el resultado en una variable $fila, llamo a la propiedad y le pido el método que llamara a la consulta de mis compañeros en fila en la base de datos
            $fila = $this->LoginProcedure->getCompañerosFilas();
            /*Se lo manda finalmente a una vista, con la variable :) esta se volvió en un arreglo asociativo, así que solo tendría que llamar a los campos desde la vista algo así {{$departamentos[0]->Codigo_Departamento}} y hacerle un foreach */           
            return view('login.lista', compact('fila'));     
        }else{
            toastr()->error('Por favor, Inicie Sesion Primero', 'Error!');
            return redirect()->action('Logins@index');
        }
    }    

}

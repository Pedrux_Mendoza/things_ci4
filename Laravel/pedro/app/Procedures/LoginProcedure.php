<?php
//Especifico la ruta donde se encuentra este archivo
namespace App\Procedures;
//Nombre de la clase
class LoginProcedure {
//Metodos que llamaran a mis consultas DB
	/*Fn: Verifica el usuario ingresado
	@param: usuario, contraseña
	@return: conteo de usuarios que se encuentra en la DB*/
    public function Verificar($user, $pass)
    {
        $user = \DB::table('usuarios')->where([
            ['user', '=', $user],
    		['pass', '=', $pass],
		])->get();
        return $user->count();
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getJoven()
    {
        return \DB::table('joven')->select('joven_id', 'nombre')->get();
    }
    /*Fn: Verifica si el id existe en la tabla hobbie
    @param: idhobbie
    @return: Valor booleano si existe en la DB*/    
    public function VerificarID($id)
    {
        $user = \DB::table('hobby')->where('hobby_id', '=', $id)->get();
        if ($user->count() > 0) {
            return "true";
        }else{
            return "false";
        }
    }    
    /*Fn: Inserta una aficion con su JOven
    @param: nombre de la aficion y el joven
    @return: Insecion de Aficiones en la DB*/
    public function insertAficion($idhobby, $nombre, $joven)
    {
        return \DB::table('hobby')->insert(['hobby_id' => $idhobby, 'nombre' => $nombre, 'id_joven' => $joven]);
    }
    /*Fn: Devuelve un listado de Jovenes con sus hobbies
    @param: na
    @return: Listados de Jovenes con sus Afciones en DB*/
    public function getCompañerosFilas()
    {
        return \DB::table('hobby')->join('joven', 'hobby.id_joven', '=', 'joven.joven_id')->select('hobby.nombre as hobby', 'joven.nombre')->whereBetween('id_joven', [4, 5])->get();;
    }            
}
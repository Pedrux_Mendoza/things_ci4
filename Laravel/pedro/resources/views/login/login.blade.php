<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet"> 
  @toastr_css   
</head>
    <body>
@include('sweet::alert')        
<div class="container login-container">
          <form method="POST" action="/verificar">
            @csrf
            <div class="row">
                <div class="col-md-6 login-form-1">
                    <h3>Acceso al sistema</h3>
                    <br>
                        <div class="form-group">
                            <input type="text" name="usuario" class="form-control" placeholder="/* User */" >
                        </div>
                        <div class="form-group">
                            <input type="password" name="contra" class="form-control" placeholder="/* Pass */">
                        </div>
                </div>
                <div class="col-md-6 login-form-2">
                        <div class="form-group">
                            <input class="btn btn-lg btn-success btn-block" type="submit" class="btnSubmit" value="Iniciar">
                        </div>
                </div>
            </div>
          </form>            
        </div>
    </body>
@jquery
@toastr_js
@toastr_render    
</html>
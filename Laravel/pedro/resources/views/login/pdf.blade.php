<html>
<head>
    <style>
        @page {
            margin: 0cm 0cm;
            font-family: Arial;
        }
 
        body {
            margin: 3cm 2cm 2cm;
        }
 
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            background-color: #3F729B;
            color: white;
            text-align: center;
            line-height: 30px;
        }
 
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            background-color: #3F729B;
            color: white;
            text-align: center;
            line-height: 35px;
        }
        .tabla{
    		width:100%;
		}
    </style>
</head>
<body>
<header>
    <h1>Reporte de aficiones CDS3</h1>
</header>
 
<main>
    <h1>Contenido</h1>
    <table border="1" class="tabla">
    	<thead>
      <tr>
        <td><b>Nombre</b></td>
        <td><b>Hobbie</b></td>   
      </tr>
      </thead>
      <tbody>
            @foreach($fila as $row)
                                <tr>
                                    <td class="text-center">{{$row->nombre}}</td>
                                    <td class="text-center">{{$row->hobby}}</td>
                                </tr>
            @endforeach 
      </tbody>
    </table>
</main>
 
<footer>
    <h1>Elaborado por: Pedro Antonio Mendoza Ramirez</h1>
</footer>
</body>
</html>
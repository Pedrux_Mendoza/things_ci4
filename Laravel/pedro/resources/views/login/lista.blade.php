<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet"> 
  @toastr_css   
</head>
    <body>
@include('sweet::alert')        
<div class="container login-container">
          <form method="POST" action="/verificar">
            @csrf
            <div class="row">
                <div class="col-md-6 login-form-1">
                    <h3>Compañeros de Filas</h3>
                    <br>
                        <div class="form-group">
                        <table border="1" class="tabla">
                            <thead>
                                <tr>
                                    <th class="text-center">Nombre</th>
                                    <th class="text-center">Hobbie</th>
                                </tr>
                            </thead>
                            <tbody>
            @foreach($fila as $row)
                                <tr>
                                    <td class="text-center">{{$row->nombre}}</td>
                                    <td class="text-center">{{$row->hobby}}</td>
                                </tr>
            @endforeach                                
                            </tbody>
                        </table>
                        </div>
                </div>
                <div class="col-md-6 login-form-2">
                        <div class="form-group">
                            <a class="btn btn-lg btn-info btn-block" href="{{ asset('/crearpdf')}}" role="button">Descargar PDF</a>  
                        </div>
                        <div class="form-group">
                            <a class="btn btn-lg btn-success btn-block" href="{{ asset('/dashboard')}}" role="button">Regresar</a>
                        </div>                        
                </div>
            </div>
          </form>            
        </div>
    </body>
@jquery
@toastr_js
@toastr_render    
</html>
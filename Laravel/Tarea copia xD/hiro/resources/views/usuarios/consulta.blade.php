<!DOCTYPE html>
<html>
<head>
	<title>Consultar Usuarios</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">   
  @toastr_css
</head>
<body>
  <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-2" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar-list-2">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="{{ route('usuarios.index') }}">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Gestion alumno
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('usuarios.create') }}">Registro de Alumnos</a>
            <a class="dropdown-item" href="#">Detalles de Alumnos</a>
          </div>
        </li>       
        <li class="nav-item active">
          <a class="nav-link" href="{{ asset('/usuarios/consulta') }}">Consulta</a>
        </li>   
      </ul>
    </div>
  </nav>
  <br>
  <div class="d-flex justify-content-center">
   <div class="col-11">
    <div class="card mb-4">
      <!-- <div class="card-header"><i class="fas fa-table mr-1"></i>Registro de Alumnos</div> -->
      <div class="card-body">
        <div class="container-fluid">
          <div class="card mb-4">
            <!-- <div class="card-header"><i class="fas fa-table mr-1"></i>Registro de Alumnos</div> -->
            <div class="card-body">
              <form method="POST" action="{{ asset('/usuarios/mostrar') }}">
                @csrf
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-sm-5 col-md-6">
                      <div class="form-group">
                        <table cellpadding="10" cellspacing="3">
                          <tr>
                            <td>Alumnos:</td>
                            <td width="100%">
                              <select name="user" class="form-control">
                                <option value="">--Seleccione--</option>
                                @foreach($usuarios as $users)
                                <option value="{{$users->usuario_id}}">{{$users->nombres}} {{$users->apellidos}}</option>
                                @endforeach                              
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">
                              <table class="tabla1">
                                <tr>
                                  <td align="left">Cantidad de Competencias</td>
                                  <td align="right" width="403px"><input type="number" name="" max="10" min="1"></td>
                                </tr>
                              </table>
                            </td>
                          </tr>                    
                        </table>
                      </div>    
                    </div>
                    <div class="col-sm-5 offset-sm-2 col-md-6 offset-md-0">
                      <div class="form-group">
                        <table cellpadding="10" cellspacing="3">
                          <tr>
                            <td>Competencia:</td>
                            <td width="100%">
                              <select name="competencia" class="form-control">
                                <option value="">--Seleccione--</option>
                                @foreach($competencias as $comp)
                                <option value="{{$comp->competencia_id}}">{{$comp->nombre}}</option>
                                @endforeach                                 
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">
                              <table class="tabla" cellspacing="3">
                                <tr>
                                  <td width="50%"><a href="#"><button type="submit" class="btn btn-block btn-lg btn-secondary">Restablecer</button></a></td>
                                  <td width="50%">
                                    <input type="submit" name="Enviar" value="Consultar" class="btn btn-block btn-lg btn-dark">
                                  </tr>
                                </table>
                              </td>
                            </tr>                          
                          </table> 
                        </div>  
                      </div>
                    </div>  
                  </div>
                </form>
              </div>
            </div>
            <table class="table table-striped table-bordered table-dark">
             <thead>
              <tr>
               <th>Nombre</th>
               <th>Operaciones</th>
             </tr>
           </thead>
           <tbody>
            @foreach($UCS as $row)    
            <tr>
              <td>{{$row->nombres}} {{$row->apellidos}}</td>
              <td><a href="#">Detalles</a> <a href="#">Deshabilitar</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</body>
@jquery
@toastr_js
@toastr_render
</html>
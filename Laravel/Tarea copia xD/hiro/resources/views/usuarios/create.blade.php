<!DOCTYPE html>
<html>
<head>
	<title>Nuevo Usuario</title>
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}"></script>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	@toastr_css	
</head>
<body>
	@include('sweet::alert')	
	<nav class="navbar navbar-dark bg-dark navbar-expand-sm">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-2" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbar-list-2">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="{{ route('usuarios.index') }}">Inicio <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item dropdown active">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Gestion alumno
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="{{ route('usuarios.create') }}">Registro de Alumnos</a>
						<a class="dropdown-item" href="#">Detalles de Alumnos</a>
					</div>
				</li>       
				<li class="nav-item">
					<a class="nav-link" href="{{ asset('/usuarios/consulta') }}">Consulta</a>
				</li>   
			</ul>
		</div>
	</nav>
	<br>
	<div class="d-flex justify-content-center">
		<div class="col-11">
			<div class="card mb-4">
				<!-- <div class="card-header"><i class="fas fa-table mr-1"></i>Registro de Alumnos</div> -->
				<div class="card-body">
					<form method="POST" action="{{ route('usuarios.store') }}">
						@csrf          	
						<div class="container-fluid">
							<div class="row">
								<div class="col-sm-5 col-md-6">
									<div class="form-group">
										<fieldset class="the-fieldset">
											<legend class="the-legend">Personales</legend>
											<table cellpadding="10" cellspacing="3">
												<tr>
													<td>Nombres:</td>
													<td width="100%"><input type="text" name="name" class="form-control"></td>
												</tr>
												<tr>
													<td>Apellidos:</td>
													<td><input type="text" name="lastname" class="form-control"></td>
												</tr>
												<tr>
													<td>Movil:</td>
													<td><input type="text" name="colonia" class="form-control" maxlength="9"></td>
												</tr>
												<tr>
													<td>Correo:</td>
													<td><input type="email" name="email" class="form-control"></td>
												</tr>					            	
											</table>
										</fieldset>
									</div>  	
								</div>
								<div class="col-sm-5 offset-sm-2 col-md-6 offset-md-0">
									<div class="form-group">
										<fieldset class="the-fieldset">
											<legend class="the-legend">Competencia</legend>
											<table cellpadding="10" cellspacing="3">
												<tr>
													<td>Nombre:</td>
													<td width="100%"><input type="text" name="competition" class="form-control"></td>
												</tr>
												<tr>
													<td>Descripcion:</td>
													<td><textarea name="description" class="form-control" rows="6"></textarea></td>
												</tr>																		
											</table>	            
										</fieldset>
									</div> 	
								</div>
							</div>	  	
						</div>
						<input type="submit" name="Enviar" value="Registrar" class="btn btn-primary float-right">
					</form>  				              	
				</div>
			</div>      
		</div>
	</div>
</body>
</html>
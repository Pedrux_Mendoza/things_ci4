<!DOCTYPE html>
<html>
<head>
	<title>Mostrar Usuarios</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">	
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">	
</head>
<body> 
  <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-2" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar-list-2">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="{{ route('usuarios.index') }}">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Gestion alumno
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('usuarios.create') }}">Registro de Alumnos</a>
            <a class="dropdown-item" href="#">Detalles de Alumnos</a>
          </div>
        </li>       
        <li class="nav-item">
          <a class="nav-link" href="{{ asset('/usuarios/consulta') }}">Consulta</a>
        </li>   
      </ul>
    </div>
  </nav>
  <br>
  <div class="d-flex justify-content-center">
   <div class="col-11">
     <div class="jumbotron bg-cover text-white" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 100%), url(https://placeimg.com/1000/480/nature)">
       <div class="container-jumbotron">
         <h1 class="display-4">Bienvenido, @Usuario!</h1>
       </div>
       <!-- /.container   -->
     </div>  
   </div>
 </div>	
</body>
</html>
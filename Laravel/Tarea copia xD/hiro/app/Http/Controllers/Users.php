<?php

namespace App\Http\Controllers;

use App\Usuarios;
use App\Procedures\UsuariosProcedure;
use Illuminate\Http\Request;

class Users extends Controller
{
    //Se almacenará en una propiedad  heredable.
    protected $UsuariosProcedure;
    //En el método constructor, se pasará para poder utilizarlo cuando desee dentro de la clase.
    /*Fn: Constructor de la Clase
    @param: Objeto de tipo UsuariosProcedure
    @return: na */    
    public function __construct(UsuariosProcedure $UsuariosProcedure)
    {
        $this->UsuariosProcedure = $UsuariosProcedure;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    
    /*Fn: Mostrar vista principal de Usuario
    @param: na
    @return: Vista Principal */ 
    public function index()
    {
        return view('usuarios.index');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*Fn: Mostrar vista del formulario de ingreso de Usuario
    @param: na
    @return: Vista del Formulario de Create */     
    public function create()
    {
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*Fn: Crea un nuevo usuario
    @param: Objeto de tipo Request
    @return: Vista Principal */     
    public function store(Request $request)
    {
        $array = [$request->name, $request->lastname, $request->colonia, $request->email, $request->competition, $request->description];
        $this->UsuariosProcedure->insertarUsuariosCompetencia($array);
        alert()->message('', 'Registro efectuado con exito')->persistent('Ok');
        return redirect()->route('usuarios.create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function consulta()
    {
        //Almaceno el resultado en una variable $usuarios, llamo a la propiedad y le pido el método que llamara al procedimiento almacenado.
        $UCS = $this->UsuariosProcedure->getUsuariosCompetencias();
        $usuarios = $this->UsuariosProcedure->getUsuarios();
        $competencias = $this->UsuariosProcedure->getCompetencias();
        /*Se lo manda finalmente a una vista, con la variable :) esta se volvió en un arreglo asociativo, así que solo tendría que llamar a los campos desde la vista algo así {{$departamentos[0]->Codigo_Departamento}} y hacerle un foreach */
        return view('usuarios.consulta', compact(['UCS', 'usuarios', 'competencias']));  
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function mostrar(Request $request)
    {
        $user = $request->user;
        $competition = $request->competencia;

        if (($user > 0) && ($competition > 0)) {
            $array = [$user, $competition];
            $UCS = $this->UsuariosProcedure->getUsuariosCompetenciaporUsuariosCompetencias($array);
            $usuarios = $this->UsuariosProcedure->getUsuarios();
            $competencias = $this->UsuariosProcedure->getCompetencias();
            toastr()->success('Consulta Ejecutada con exito', 'Exito!');
        }else{
            if ($user > 0) {
                $array = [$user];
                $UCS = $this->UsuariosProcedure->getUsuariosCompetenciaporUsuario($array);
                $usuarios = $this->UsuariosProcedure->getUsuarios();
                $competencias = $this->UsuariosProcedure->getCompetencias();
                toastr()->success('Consulta Ejecutada con exito', 'Exito!');
            }else{
                if ($competition > 0) {
                    $array = [$competition];
                    $UCS = $this->UsuariosProcedure->getUsuariosCompetenciaporCompetencia($array);
                    $usuarios = $this->UsuariosProcedure->getUsuarios();
                    $competencias = $this->UsuariosProcedure->getCompetencias();
                    toastr()->success('Consulta Ejecutada con exito', 'Exito!');     
                }else{
                    $UCS = $this->UsuariosProcedure->getUsuariosCompetencias();
                    $usuarios = $this->UsuariosProcedure->getUsuarios();
                    $competencias = $this->UsuariosProcedure->getCompetencias();
                    toastr()->error('Seleccione un Alumno o Competencia para realizar la consulta', 'Error!');
                }
            }
        }

        return view('usuarios.consulta', compact(['UCS', 'usuarios', 'competencias']));  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(Usuarios $usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuarios $usuarios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usuarios $usuarios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuarios $usuarios)
    {
        //
    }
}

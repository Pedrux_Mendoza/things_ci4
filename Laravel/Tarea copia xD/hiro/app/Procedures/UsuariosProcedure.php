<?php
//Especifico la ruta donde se encuentra este archivo
namespace App\Procedures;
//Nombre de la clase
class UsuariosProcedure {
//Metodos que llamaran a mis procedimientos
	/*Fn: Devuelve un listado de usuarios con competencias
	@param: na
	@return: Ejecucion de procedimiento almacenado*/
    public function getUsuariosCompetencias()
    {
        return \DB::select('CALL sp_consultar_user_competition');
    }
    /*Fn: Devuelve un listado de usuarios
    @param: na
    @return: Ejecucion de procedimiento almacenado*/
    public function getUsuarios()
    {
        return \DB::select('CALL sp_mostrarUsuario');
    }
    /*Fn: Devuelve un listado de competencias
    @param: na
    @return: Ejecucion de procedimiento almacenado*/
    public function getCompetencias()
    {
        return \DB::select('CALL sp_mostrarCompetencias');
    }        
	/*Fn: Agregar un usuario y su competencia
	@param: arreglo de datos
	@return: Ejecucion de procedimiento almacenado de insercion*/
    public function insertarUsuariosCompetencia($array)
    {
        return \DB::select('CALL sp_insertar_UC(?,?,?,?,?,?)', $array);
    }
    /*Fn: Agregar un usuario y su competencia por medio del id del Usuario
    @param: arreglo de datos
    @return: Ejecucion de procedimiento almacenado*/
    public function getUsuariosCompetenciaporUsuario($array)
    {
        return \DB::select('CALL sp_consultar_UC1(?)', $array);
    }
    /*Fn: Agregar un usuario y su competencia por medio del id de la Competencia
    @param: arreglo de datos
    @return: Ejecucion de procedimiento almacenado*/
    public function getUsuariosCompetenciaporCompetencia($array)
    {
        return \DB::select('CALL sp_consultar_UC2(?)', $array);
    } 
    /*Fn: Agregar un usuario y su competencia por medio del id del Usuario y id de la Competencia
    @param: arreglo de datos
    @return: Ejecucion de procedimiento almacenado*/
    public function getUsuariosCompetenciaporUsuariosCompetencias($array)
    {
        return \DB::select('CALL sp_consultar_UC3(?,?)', $array);
    }       
}